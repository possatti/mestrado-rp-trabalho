#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Example on how to use Keras. I followed this tutorial:
 - http://machinelearningmastery.com/tutorial-first-neural-network-python-keras/

You can install every necessary thing on Anaconda like this:
 - conda create -n keras python=3 numpy tensorflow keras

And use the new environment:
 - source activate keras
"""

from __future__ import print_function, division
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Activation

import numpy as np
import argparse
import sys
import re

print(sys.version, file=sys.stderr)

def argparse_endswith(ext):
	def evaluate_file_path(file_path):
		if file_path.endswith(ext):
			return file_path
		else:
			raise argparse.ArgumentTypeError('\'{}\' does not have the \'{}\' extension.'.format(file_path, ext))
	return evaluate_file_path

def main():
	# Fix random seed for reproducibility.
	np.random.seed(7)

	print('Preparing dataset...', file=sys.stderr)
	dataset = np.loadtxt(args.dataset_file, delimiter=",")
	np.random.shuffle(dataset)

	# Split taining and testing data.
	training_dataset = dataset[:600]
	validation_dataset = dataset[600:700]
	testing_dataset = dataset[700:]

	# Split inputs and outputs.
	training_input = training_dataset[:,0:8]
	training_output = training_dataset[:,8]
	validation_input = validation_dataset[:,0:8]
	validation_output = validation_dataset[:,8]
	testing_input = testing_dataset[:,0:8]
	testing_output = testing_dataset[:,8]

	# Load the model, or not.
	if args.load_model:
		print('Loading model from `{}`...'.format(args.load_model), file=sys.stderr)
		with open('model.json', 'r') as json_file:
			loaded_model_json = json_file.read()
		model = model_from_json(loaded_model_json)
		print('Loaded model from disk.', file=sys.stderr)
	else:
		print('Creating model from scratch...', file=sys.stderr)
		model = Sequential()
		model.add(Dense(12, input_dim=8, activation='relu'))
		model.add(Dense(8, activation='relu'))
		model.add(Dense(8, activation='relu'))
		model.add(Dense(1, activation='sigmoid'))

	# Load the weights, or not.
	if args.load_weights:
		print('Loading weights from `{}`...'.format(args.load_weights), file=sys.stderr)
		model.load_weights(args.load_weights)
		print('Loaded weights from disk.', file=sys.stderr)
		print('Compiling loaded model...', file=sys.stderr)
		model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
		print('Compiled.', file=sys.stderr)
	else:
		print('Compiling model...', file=sys.stderr)
		model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

		print('Training...', file=sys.stderr)
		model.fit(training_input, training_output, epochs=10, batch_size=10)

	print('Evaluating...', file=sys.stderr)
	scores = model.evaluate(validation_input, validation_output)
	print()
	for metric_name, score in zip(model.metrics_names, scores):
		print('{}: {:.4f}'.format(metric_name, score))

	# Save the trained model
	if args.save_model:
		print('Saving the model structure to `{}`...'.format(args.save_model), file=sys.stderr)
		model_json = model.to_json()
		with open(args.save_model, "w") as json_file:
			json_file.write(model_json)
		print('Model saved.', file=sys.stderr)
	if args.save_weights:
		print('Saving model weights to {}...'.format(args.save_weights), file=sys.stderr)
		model.save_weights(args.save_weights)
		print('Saved weights to disk.', file=sys.stderr)

	print('Predicting outputs from test data...', file=sys.stderr)
	predictions = model.predict(testing_input)
	for instance, prediction, ground_truth in zip(testing_input, predictions, testing_output):
		instance_str = ''
		for x in instance:
			instance_str += ' {:>3.2f}'.format(x)
		print('Instance:{}; Prediction: {}; Ground Truth: {}'.format(instance_str, prediction, ground_truth))

# Arguments and options
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('dataset_file', nargs='?', default='pima-indians-diabetes.csv', help='CSV with the dataset data.')
	parser.add_argument('--load-model', type=argparse_endswith('.json'))
	parser.add_argument('--load-weights', type=argparse_endswith('.h5'))
	parser.add_argument('--save-model', type=argparse_endswith('.json'))
	parser.add_argument('--save-weights', type=argparse_endswith('.h5'))
	args = parser.parse_args()
	print(args)
	main()