#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import Popen, PIPE, call

import argparse
import shutil
import sys
import re
import os


def run(input_file_path, cstr='cstr3', print_stdout=False, print_stderr=False):
	"""Runs a single input file. Useful for testing."""
	# Open the input source.
	with open(input_file_path, 'r') as f:
		content = f.read()

	# Remove comments from the source.
	content = re.sub(r'^##.*?\n', '', content, flags=re.MULTILINE)
	content = re.sub(r'\s*#.*$', '', content, flags=re.MULTILINE)

	# Run cstr3 with the specified input.
	cstr3_process = Popen([cstr], stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout_data, stderr_data = cstr3_process.communicate(input=content.encode('utf-8'))

	# Print the output of the run.
	if print_stdout: print(stdout_data.decode())
	if print_stderr: print(stderr_data.decode(), file=sys.stderr)

	return stdout_data.decode(), stderr_data.decode()

def build_all():
	"""Builds a single CSV with all data from all simulations."""

	# Find simulation files.
	input_files = [os.path.join(args.simulations_folder, fp) for fp in os.listdir(args.simulations_folder) if fp.endswith('.txt') ]

	# Clean the output folder.
	shutil.rmtree(args.output_path, ignore_errors=True)

	for input_file_path in input_files:
		stdout_text, stderr_text = run(input_file_path, cstr=args.cstr3)

		# Move files to a nice folder of its own.
		simulation_name = os.path.basename(input_file_path)
		folder_path = os.path.join(args.output_path, simulation_name)
		os.makedirs(folder_path, exist_ok=True)
		for fp in 'DUMP.RAW', 'X.csv', 'log.txt':
			os.rename(fp, os.path.join(folder_path, fp))

	# Find all 'X.csv' files.
	X_files = []
	for path, folders, files in os.walk(args.output_path):
		for file_name in files:
			file_path = os.path.join(path, file_name)
			if file_path.endswith('X.csv'):
				X_files.append(file_path)

	# Gather all their data.
	all_data = []
	for X_path in X_files:
		with open(X_path, 'r') as f:
			lines = f.readlines()
			data_lines = list(filter(lambda l: not re.match(r'^18$', l), lines))
			data_split = list(map(lambda l: re.split(r';\s*', l.strip()), data_lines))
			all_data += data_split

	print('Number of instances:', len(all_data), file=sys.stderr)
	coutings = {}
	for instance in all_data:
		if instance[-1] in coutings:
			coutings[instance[-1]] += 1
		else:
			coutings[instance[-1]] = 0
	print('Class couting: {}'.format(coutings), file=sys.stderr)

	# Save all data to a single file.
	with open(args.compiled_dataset_path, 'w') as f:
		for instance in all_data:
			f.write(','.join(instance)+'\n')

	print('All `X.csv` files compiled to `{}`.'.format(args.compiled_dataset_path), file=sys.stderr)
	print('Done.', file=sys.stderr)

def main():
	if args.command == 'run':
		run(args.input_file, cstr=args.cstr3, print_stdout=True, print_stderr=True)
	elif args.command == 'build':
		build_all()


if __name__ == '__main__':
	# Default paths.
	default_cstr3_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'cstr3', 'cstr3'))
	default_simulations_folder = os.path.realpath(os.path.join(os.path.dirname(__file__), 'simulations'))
	default_output_path = os.path.realpath(os.path.join(os.path.dirname(__file__), 'simulations', 'output'))
	default_compilation_path = os.path.realpath(os.path.join(os.path.dirname(__file__), 'dataset.csv'))

	# Arguments and options
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('-c', '--cstr3-path', dest='cstr3', default=default_cstr3_path, help='Path to which cstr3 binary should be used. The default assumes `cstr3` is on the path.')
	parser.add_argument('-d', '--debug', action='store_true', help='Print debug messages.')
	subparsers = parser.add_subparsers(help='Available commands.', dest='command')

	# 'run' command.
	run_parser = subparsers.add_parser('run', help='Run a certain input file on the cstr3.')
	run_parser.add_argument('input_file', help='Which file should be run.')

	# 'build' command.
	build_parser = subparsers.add_parser('build', help='Build all the input files.')
	build_parser.add_argument('-s', '--simulations-folder', default=default_simulations_folder, help='Where the simulations folder is.')
	build_parser.add_argument('-p', '--compiled-dataset-path', default=default_compilation_path, help='Where to save the compiled dataset.')
	build_parser.add_argument('-o', '--output-path', default=default_output_path, help='Where to save the intermediate build files.')

	args = parser.parse_args()
	if not args.command:
		parser.print_help(file=sys.stderr)
	main()
