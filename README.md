Trabalho de Reconhecimento de Padrões
=====================================

Essa é a nossa solução para o trabalho de Reconhecimento de Padrões da turma de 2017-1. A especificação do trabalho se encontra em `trabalho.pdf`.

## Alunos
 - Lucas Caetano Possatti
 - Rafael Horimoto de Freitas

## Dependências

Para compilar o `cstr3` é necessário um compilador de fortran, nós utilizamos o `gfortran`.

Para manipular o dataset e usar os modelos de classificação que utilizamos é necessário instalar o Python 3 (talvez o Python 2 funcione também), e as seguintes bibliotecas: Numpy, Scikit Learn, Keras, e Matplotlib. Opcionalmente você pode instalar o Tensorflow para servir de backend para o Keras, esse foi o backend que utilizamos.

Você pode instalar essas bibliotecas facilmente usando a [distribuição Anaconda](https://www.continuum.io/downloads), que instala o Python juntamente com o Matplotlib, o Numpy, o Scikit Learn, e outras bibliotecas científicas. E então instalar o Keras e Tensorflow separadamente usando o comando `conda install keras tensorflow`.

Ou se você já tem o Python instalado, você pode instalar as bibliotecas necessárias usando o comando `pip install matplotlib numpy scikit-learn keras tensorflow`, porém não testamos essa opção.

## Instruções de uso

Primeiro compile o `cstr3`, esse comandos deveriam bastar (dado que você tem o `gfortran` instalado):

```
cd cstr3
make
```

Em seguida, construa o dataset usando os seguintes comandos:

```
python dataset/dataset-manager.py build
```

O `dataset-manager.py` irá usar os arquivos de simulações em `dataset/simulations` para executar o `cstr3` e criar um único arquivo com o resultado de todas as simulações em `dataset/dataset.csv`.

Quando você tiver o dataset pronto, já pode executar qualquer um dos modelos do diretório `models/`. Exemplos:

```
python models/svm.py
python models/knn.py
python models/mlp.py
python models/dnn.py
```

Todos esses modelos assumem que você criou o dataset da forma especificada.

O script `models/dnn-search.py` é usado para buscar os melhores hyper-parâmetros para o modelo de uma rede neural. A saída do script é um relatório no formato `csv`, que mostra um ranking dos melhores hyper-parâmetros. Nós fizemos algumas buscas usando esse script e você encontra os resultados nos arquivos `models/dnn-search--*.csv`.

As figuras do diretório `models/` mostram o treinamento usando três redes diferentes. Mas são apenas rascunhos, e não temos o código que usamos para gerar essas figuras.

Outra parte do trabalho envolve a Análise de Componentes Principais (PCA). No diretório `pca/`, temos alguns scripts para fazer o PCA usando algoritmos diferentes. O que nós utilizamos para o trabalho mesmo foi o `pca/pca.py`.

O `pca/pca.py` e o `pca/pca-possatti.py` são implementações manuais que nós fizemos. O `pca/pca-matplotlib.py` usa a implementação do PCA do Matplotlib. E o `pca/pca-sk.py` usa a implementação do Scikit Learn. Todos os scripts tem uma interface parecida.

Exemplos de uso do `pca/pca.py`:

```
python pca/pca.py pca/examples/X.csv --format XCSV
python pca/pca.py dataset/dataset.csv
```
