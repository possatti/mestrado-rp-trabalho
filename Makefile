#!/usr/bin/make -f
SHELL=/bin/bash
.ONESHELL:
.PHONY: all clean

ZIP_PACKAGE = rp-trabalho.zip
ZIP_SOURCE_FILES = cstr3 dataset models pca README.md trabalho.pdf

all: $(ZIP_PACKAGE)

$(ZIP_PACKAGE): $(ZIP_SOURCE_FILES)
	zip -r $(@) $(?)

clean:
	rm -f $(ZIP_PACKAGE)
