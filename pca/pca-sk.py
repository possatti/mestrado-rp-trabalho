#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Important links.

PCA:
 - https://www.clear.rice.edu/comp130/12spring/pca/pca_docs.shtml
 - http://blog.nextgenetics.net/?e=42
 - https://matplotlib.org/api/mlab_api.html#matplotlib.mlab.PCA

Matplotlib:
 - http://stackoverflow.com/questions/23688227/confusion-about-artist-place-in-matplotlib-hierarchy
'''

from __future__ import print_function
from sklearn.decomposition import PCA
from itertools import cycle
from io import StringIO

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import re

# Colors and markers that can be used with matplotlib.
COLORS = cycle(['blue', 'red', 'yellow', 'black', 'green', 'cyan', 'magenta'])
MARKERS = cycle(['o', 's', '^', 'p', 'h', '8', 'D', 'x'])

def main():
	np.random.seed(7)

	# Read data from file.
	if args.format == 'CSV':
		data_bytes = np.loadtxt(args.data_file, delimiter=',', dtype=bytes)
	elif args.format == 'XCSV':
		data_bytes = np.loadtxt(args.data_file, delimiter=';', skiprows=1, dtype=bytes)
	data = data_bytes[:,:-1].astype(float)
	labels = data_bytes[:,-1].astype(str)
	labels_unique = np.unique(labels)

	# The results from applying the PCA.
	pca = PCA()
	results = pca.fit_transform(data)

	if args.graph_3d:
		# Prepare 3D plot
		from mpl_toolkits.mplot3d import Axes3D
		fig = plt.figure()
		ax = Axes3D(fig)

		# For each label, plot the points on graph.
		for i, label in enumerate(labels_unique):
			label_results = results[labels == label]
			ax.scatter(label_results[:,0], label_results[:,1], label_results[:,2], s=30, c=next(COLORS), marker=next(MARKERS), label=label)

		ax.legend()
		ax.set_xlabel('PCA1')
		ax.set_ylabel('PCA2')
		ax.set_zlabel('PCA3')
	else:
		for label in labels_unique:
			label_results = results[labels == label]
			plt.scatter(label_results[:,0], label_results[:,1], s=30, c=next(COLORS), marker=next(MARKERS), label=label)
		plt.legend()
		plt.title(args.data_file)
		plt.xlabel('PC 1')
		plt.ylabel('PC 2')

	# Save the figure to a file, or show it on screen.
	if args.save:
		plt.savefig(args.save)
	else:
		plt.show()

# Arguments and options
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('data_file', help='File containing the data for the PCA.')
	parser.add_argument('-f', '--format', choices=['CSV', 'XCSV'], default='CSV', help='Format of the file to be read (defaults to CSV).')
	parser.add_argument('-3', '--graph-3d', action='store_true', help='Plot it 3D, instead of 2D.')
	parser.add_argument('-s', '--save', metavar='file_path', help='Save the plot to this file.')
	args = parser.parse_args()
	main()
