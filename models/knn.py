#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.metrics import classification_report
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import KFold, train_test_split

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

NUM_CLASSES = 23

def main():
	# Prepare data.
	np.random.seed(7)
	dataset_str = np.loadtxt(args.dataset, delimiter=',', dtype=bytes)
	np.random.shuffle(dataset_str)
	dataset_data = dataset_str[:,:-1].astype(float)
	dataset_labels = dataset_str[:,-1].astype(str)

	# Counts the number of instances.
	labels_unique, label_counts = np.unique(dataset_labels, return_counts=True)
	print('Total number of instances:', len(dataset_labels), file=sys.stderr)
	print('Number of instances for each label:\n', dict(zip(labels_unique, label_counts)), file=sys.stderr)

	# K Nearest Neighbors
	ks = range(1,10)
	kfold = KFold(n_splits=5, shuffle=True)
	for train, test in kfold.split(dataset_data, dataset_labels):
		X_train, X_val, y_train, y_val = train_test_split(dataset_data[train], dataset_labels[train], train_size=0.8)
		best_acc = 0
		best_k = 1
		for k in ks:
			clf = KNeighborsClassifier(k, weights='uniform')
			clf.fit(dataset_data[train], dataset_labels[train])
			acc = clf.score(dataset_data[test], dataset_labels[test])
			if acc > best_acc:
				best_acc = acc
				best_k = k
		print('Best K:', best_k)
		clf = KNeighborsClassifier(best_k, weights='uniform')
		clf.fit(dataset_data[train], dataset_labels[train])
		acc = clf.score(dataset_data[test], dataset_labels[test])
		print('acc:', acc)


if __name__ == '__main__':
	# Defaults.
	default_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'dataset', 'dataset.csv'))

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dataset-path', dest='dataset', default=default_dataset_path)
	args = parser.parse_args()
	main()
