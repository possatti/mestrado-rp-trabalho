#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD, Adam, Adamax
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

import pandas as pd
import numpy as np
import argparse
import types
import sys
import re
import os

INPUT_DIMENSIONS = 18
SEED = 7

print(sys.version, file=sys.stderr)

def patch_optimizer(opt):
	"""Patches the optimizer to print the learning rate and stuff."""
	def print_me(opt, fmt=None):
		name = re.match(r"<class 'keras\.optimizers\.(\w+)'>", str(type(opt))).group(1)
		return '{}(lr={lr:.6f})'.format(name, **opt.get_config())
	opt.__str__ = types.MethodType(print_me, opt)
	opt.__repr__ = types.MethodType(print_me, opt)
	opt.__format__ = types.MethodType(print_me, opt)
	return opt

activation_optimizer_search_params = {
	'architecture': ['200-150-100-50'],
	'optimizer': ['adam', 'adamax', 'nadam', 'sgd', 'rmsprop'],
	'activation': ['relu', 'sigmoid', 'tanh', 'linear'],
}

learning_rate_search_params = {
	'architecture': ['200-150-100-50'],
	'optimizer': [
		patch_optimizer(Adam(lr=0.01)),
		patch_optimizer(Adam(lr=0.001)), # Default.
		patch_optimizer(Adam(lr=0.0001)),
		patch_optimizer(Adam(lr=0.00001)),
		patch_optimizer(SGD(lr=0.01)), # Default.
		patch_optimizer(SGD(lr=0.001)),
		patch_optimizer(SGD(lr=0.0001)),
		patch_optimizer(SGD(lr=0.00001)),
		patch_optimizer(Adamax(lr=0.02)),
		patch_optimizer(Adamax(lr=0.002)), # Default.
		patch_optimizer(Adamax(lr=0.0002)),
	],
	'activation': ['relu'],
}

architecture_search_params = {
	'architecture': [
		'128',
		'128-128',
		'128-64',
		'128-64-32',
		'200-150-100-50-30',
		'200-150-100-50',
		'50-50-50-50',
		'20-20-20-20',
		'200-100-50-30',
		'200-100-50',
		'100-50',
	],
	'optimizer': ['adam'],
	'activation': ['relu'],
}

epochs_search_params = {
	'architecture': ['200-150-100-50'],
	'optimizer': ['adam'],
	'activation': ['relu'],
	'epochs': [
		100,
		250,
		500,
		750,
		1000,
	]
}

random_search_params = {
	'architecture': [
		'128-128',
		'200-150-100-50',
		'200-150-100-50-30',
	],
	'optimizer': ['adam', 'sgd', 'adamax'],
	'activation': ['relu', 'tanh', 'linear', 'sigmoid'],
	'epochs': [
		100,
		250,
		500,
		750,
		1000,
	]
}

params_of_interest = [
	'param_architecture',
	'param_activation',
	'param_architecture',
	'param_optimizer',
	'param_lr',
]

def baseline_model(input_dimensions=INPUT_DIMENSIONS, num_classes=11, architecture=(128,128), loss='categorical_crossentropy', optimizer='adam', activation='relu'):
	print('loss: {}; act: {}; opt: {}; arch: {}; lr: {:.7}'.format(loss, activation, optimizer, architecture, 'default' if type(optimizer) is str else optimizer.get_config()['lr']), file=sys.stderr)

	# Breakdown architecture if it is a string like '20-10-5'.
	if type(architecture) is str:
		architecture = [ int(n) for n in architecture.split('-') ]

	model = Sequential()
	model.add(Dense(architecture[0], input_dim=input_dimensions, activation=activation))
	for n_nodes in architecture[1:]:
		model.add(Dense(n_nodes, activation=activation))
	model.add(Dense(num_classes, activation='softmax'))
	model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

	return model

def main():
	# Load data.
	np.random.seed(SEED)
	dataset_str = np.loadtxt(args.dataset, delimiter=',', dtype=bytes)
	np.random.shuffle(dataset_str)
	data = dataset_str[:,:-1].astype(float)
	labels = dataset_str[:,-1].astype(str)

	# Data normalization (all attr are fitted to the range 0-1).
	minimums = data.min(axis=0)
	maximums = data.max(axis=0)
	data = (data - minimums) / (maximums - minimums)

	# Transform labels to a one-hot vector.
	encoder = LabelEncoder()
	encoded_labels = encoder.fit_transform(labels)
	categorical_labels = np_utils.to_categorical(encoded_labels)

	# Counts the number of instances.
	labels_unique, label_counts = np.unique(labels, return_counts=True)
	print('Total number of instances:', len(labels), file=sys.stderr)
	print('Number of unique classes:', len(labels_unique), file=sys.stderr)
	print('Number of instances for each label:\n', dict(zip(labels_unique, label_counts)), file=sys.stderr)

	# Do a grid search for best hyper parameters.
	print('Searching for best hyper-parameters.', file=sys.stderr)
	estimator = KerasClassifier(build_fn=baseline_model, epochs=100, batch_size=200, verbose=0)
	# grid = GridSearchCV(estimator, activation_optimizer_search_params, n_jobs=args.threads, verbose=1, refit=False)
	# grid = GridSearchCV(estimator, architecture_search_params, n_jobs=args.threads, verbose=1, refit=False)
	# grid = GridSearchCV(estimator, learning_rate_search_params, n_jobs=args.threads, verbose=1, refit=False)
	# grid = GridSearchCV(estimator, epochs_search_params, n_jobs=args.threads, verbose=1, refit=False)
	grid = RandomizedSearchCV(estimator, random_search_params, n_iter=10, n_jobs=args.threads, verbose=1, refit=False)
	grid.fit(data, categorical_labels)

	# Save results from the grid search.
	print('Saving report to `{}`.'.format(args.save_report), file=sys.stderr)
	results = pd.DataFrame(grid.cv_results_)
	results.sort_values(by='rank_test_score', inplace=True)
	results = results.assign(param_optimizer=lambda df: list(map(lambda opt: opt if type(opt) is str else opt.__str__(), df.param_optimizer))) # Pretty print optimizer.
	param_cols = list(filter(re.compile(r'^param_').match, results.columns)) # Choose parameters.
	results.to_csv(args.save_report, index=False, float_format='%.4f',
		columns=['rank_test_score', 'mean_train_score', 'mean_test_score', 'std_test_score'] + param_cols)


if __name__ == '__main__':
	# Defaults.
	default_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'dataset', 'dataset.csv'))
	default_report_path = __file__.replace('.py', '-report.csv')

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dataset-path', dest='dataset', default=default_dataset_path)
	# parser.add_argument('-k', '--kfold', nargs='?', const=10)
	parser.add_argument('-s', '--save-report', metavar='CSV_PATH', default=default_report_path)
	parser.add_argument('-t', '--threads', type=int, default=-1, help='Number of threads to use. Default is `-1, i.e. the number of cores.`')
	args = parser.parse_args()
	main()
