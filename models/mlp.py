#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import normalize

import numpy as np
import functools
import argparse
import sys
import os

INPUT_DIMENSIONS = 18
SEED = 7

print(sys.version, file=sys.stderr)

def baseline_model(input_dimensions, num_classes):
	model = Sequential()
	model.add(Dense(128, input_dim=input_dimensions, activation='relu'))
	# model.add(Dropout(0.1))
	model.add(Dense(128, activation='relu'))
	# model.add(Dropout(0.1))
	model.add(Dense(num_classes, activation='softmax'))
	model.summary()
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

def main():
	np.random.seed(SEED)
	dataset_str = np.loadtxt(args.dataset, delimiter=',', dtype=bytes)
	np.random.shuffle(dataset_str)
	data = dataset_str[:,:-1].astype(float)
	labels = dataset_str[:,-1].astype(str)

	# Data normalization (all attr are fitted to the range 0-1).
	minimums = data.min(axis=0)
	maximums = data.max(axis=0)
	data = (data - minimums) / (maximums - minimums)

	# One-hot stuff.
	encoder = LabelEncoder()
	encoder.fit(labels)
	encoded_labels = encoder.transform(labels)
	categorical_labels = np_utils.to_categorical(encoded_labels)

	# Counts the number of instances.
	labels_unique, label_counts = np.unique(labels, return_counts=True)
	print('Total number of instances:', len(labels), file=sys.stderr)
	print('Number of instances for each label:\n', dict(zip(labels_unique, label_counts)), file=sys.stderr)

	if args.kfold:
		print('Training using K-Fold, it might take a while...', file=sys.stderr)
		baseline_model_binded = functools.partial(baseline_model, INPUT_DIMENSIONS, len(labels_unique))
		estimator = KerasClassifier(build_fn=baseline_model_binded, epochs=1000, batch_size=200, verbose=0)
		kfold = KFold(n_splits=args.kfold, shuffle=True)
		accs = cross_val_score(estimator, data, categorical_labels, cv=kfold)
		print('Acurracies:', accs)
		print("Average Accuracy: %.2f%% (%.2f%%)" % (accs.mean()*100, accs.std()*100))
	else:
		# Split dataset into training, validation and testing.
		splits = [int(len(dataset_str)*0.6), int(len(dataset_str)*0.8)]
		training_data, validation_data, testing_data = np.split(data, splits)
		training_labels, validation_labels, testing_labels = np.split(labels, splits)
		training_cat_labels, validation_cat_labels, testing_cat_labels = np.split(categorical_labels, splits)

		print('Training...', file=sys.stderr)
		model = baseline_model(input_dimensions=INPUT_DIMENSIONS, num_classes=len(labels_unique))
		model.fit(training_data, training_cat_labels,
			validation_data=(validation_data, validation_cat_labels),
			epochs=1000, batch_size=200)

		print('Evaluating...', file=sys.stderr)
		scores = model.evaluate(testing_data, testing_cat_labels)
		print('\nTesting scores:')
		for metric_name, score in zip(model.metrics_names, scores):
			print(' - {}: {:.4f}'.format(metric_name, score))


if __name__ == '__main__':
	# Defaults.
	default_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'dataset', 'dataset.csv'))

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dataset-path', dest='dataset', default=default_dataset_path)
	parser.add_argument('-k', '--kfold', nargs='?', const=10)
	args = parser.parse_args()
	main()
