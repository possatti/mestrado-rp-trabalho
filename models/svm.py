#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.model_selection import KFold, train_test_split, cross_val_score
from sklearn.svm import SVC

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

NUM_CLASSES = 23

def main():
	# Read data.
	np.random.seed(7)
	dataset = np.loadtxt(args.dataset, delimiter=',', dtype=bytes)
	np.random.shuffle(dataset)
	data = dataset[:,:-1].astype(float)
	labels = dataset[:,-1].astype(str)

	# Normalization.
	minimums = data.min(axis=0)
	maximums = data.max(axis=0)
	data = (data - minimums) / (maximums - minimums)

	# Counts the number of instances.
	labels_unique, label_counts = np.unique(labels, return_counts=True)
	print('Total number of instances:', len(labels), file=sys.stderr)
	print('Number of instances for each label:\n', dict(zip(labels_unique, label_counts)), file=sys.stderr)

	# Training with cross-validation.
	print('Doing K-Fold cross-validation with K = {}.'.format(args.k))
	print()
	for kernel in ['rbf', 'poly', 'sigmoid', 'linear']:
		print('Using {} kernel.'.format(kernel))
		svc = SVC(kernel=kernel)
		kfold = KFold(n_splits=args.k, shuffle=True)
		scores = cross_val_score(svc, data, labels, cv=kfold, verbose=0)
		print('Scores:', scores)
		print('Mean score:', scores.mean())
		print()


if __name__ == '__main__':
	# Defaults.
	default_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'dataset', 'dataset.csv'))

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dataset-path', dest='dataset', default=default_dataset_path)
	parser.add_argument('-k', '--kfold', metavar='K', dest='k', type=int, default=10, help='Use KFold cross-validation with the specified `K` number of folds.')
	args = parser.parse_args()
	main()
