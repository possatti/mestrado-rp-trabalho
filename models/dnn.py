#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD, Adam, Adamax
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
from sklearn.dummy import DummyClassifier

import numpy as np
import argparse
import types
import sys
import os

INPUT_DIMENSIONS = 18
SEED = 7

print(sys.version, file=sys.stderr)

def log_and_print(*args, file=None):
	print(*args)
	if file is not None:
		print(*args, file=file)

def baseline_model(input_dimensions=INPUT_DIMENSIONS, num_classes=11, architecture=(128,128), loss='categorical_crossentropy', optimizer='adam', activation='relu'):
	print('loss: {}; act: {}; opt: {}; arch: {}; lr: {:.7}'.format(loss, activation, optimizer, architecture, 'default' if type(optimizer) is str else optimizer.get_config()['lr']), file=sys.stderr)

	# Breakdown architecture if it is a string like '20-10-5'.
	if type(architecture) is str:
		architecture = [ int(n) for n in architecture.split('-') ]

	model = Sequential()
	model.add(Dense(architecture[0], input_dim=input_dimensions, activation=activation))
	for n_nodes in architecture[1:]:
		model.add(Dense(n_nodes, activation=activation))
	model.add(Dense(num_classes, activation='softmax'))
	model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

	return model

def main():
	# Load data.
	np.random.seed(SEED)
	dataset_str = np.loadtxt(args.dataset, delimiter=',', dtype=bytes)
	np.random.shuffle(dataset_str)
	data = dataset_str[:,:-1].astype(float)
	labels = dataset_str[:,-1].astype(str)

	# Data normalization (all attr are fitted to the range 0-1).
	minimums = data.min(axis=0)
	maximums = data.max(axis=0)
	data = (data - minimums) / (maximums - minimums)

	# Transform labels to a one-hot vector.
	encoder = LabelEncoder()
	encoded_labels = encoder.fit_transform(labels)
	categorical_labels = np_utils.to_categorical(encoded_labels)

	# Counts the number of instances.
	labels_unique, label_counts = np.unique(labels, return_counts=True)
	print('Total number of instances:', len(labels), file=sys.stderr)
	print('Number of unique classes:', len(labels_unique), file=sys.stderr)
	print('Number of instances for each label:\n', dict(zip(labels_unique, label_counts)), file=sys.stderr)

	estimators = {
		'dummy (stractified)': DummyClassifier(),
		'dummy (most frequent)': DummyClassifier(strategy='most_frequent'),
		'200-150-100-50 adam tanh': KerasClassifier(build_fn=baseline_model,
			architecture='200-150-100-50', optimizer='adam', activation='tanh',
			epochs=1000, batch_size=200, verbose=0),
		'200-150-100-50 adam relu': KerasClassifier(build_fn=baseline_model,
			architecture='200-150-100-50', optimizer='adam', activation='relu',
			epochs=1000, batch_size=200, verbose=0),
		'128-128 adam relu': KerasClassifier(build_fn=baseline_model,
			architecture='128-128', optimizer='adam', activation='relu',
			epochs=1000, batch_size=200, verbose=0),
	}

	with open(__file__.replace('.py', '.log'), 'w') as log_file:
		print('\nTraining using K-Fold, it might take a while...', file=sys.stderr)
		for description, estimator in estimators.items():
			log_and_print('Model:', description, file=log_file)
			accs = cross_val_score(estimator, data, categorical_labels, cv=args.kfold, n_jobs=args.threads, verbose=1)
			log_and_print('Acurracies:', accs, file=log_file)
			log_and_print("Average accuracy: {:.2f}% (std: {:.2f}%)".format(accs.mean()*100, accs.std()*100), file=log_file)
			log_and_print(file=log_file)

if __name__ == '__main__':
	# Defaults.
	default_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'dataset', 'dataset.csv'))

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dataset-path', dest='dataset', default=default_dataset_path)
	parser.add_argument('-k', '--kfold', metavar='K', type=int, default=3)
	parser.add_argument('-t', '--threads', type=int, default=-1, help='Number of threads to use. Default is `-1, i.e. the number of cores.`')
	args = parser.parse_args()
	main()
